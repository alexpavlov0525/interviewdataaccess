﻿namespace Domain.DataStorage
{
    public static class StoredProcedures
    {
        public static string GetPeopleWithOrderOnDate = "getAllPeopleWithOrdersOnDate";

        public static bool Exists(string target)
        {
            if (GetPeopleWithOrderOnDate != target)
            {
                return false;
            }

            return true;
        }
    }
}
