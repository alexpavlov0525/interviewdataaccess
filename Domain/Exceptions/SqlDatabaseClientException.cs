﻿using System;

namespace Domain.Exceptions
{
    public class SqlDatabaseClientException : Exception
    {
        public SqlDatabaseClientException(string message) : base(message)
        {
        }

        public SqlDatabaseClientException(string message, Exception exception) : base(message, exception)
        {
        }
    }
}
