﻿using System;

namespace Domain.Exceptions
{
    public class MappingFunctionException : Exception
    {
        public MappingFunctionException(string message, Exception exception) :  base(message, exception)
        {
        }
    }
}
