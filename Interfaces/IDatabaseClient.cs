﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace DataStorage
{
    /// <summary>
    /// Sql database client
    /// </summary>
    public interface IDatabaseClient
    {
        /// <summary>
        /// Executes a stored procedure which is suppose to read data.
        /// </summary>
        /// <typeparam name="T">Type of the objects in returned collection</typeparam>
        /// <param name="storedProcedure">Stored procedure name</param>
        /// <param name="parameters">Collection of Sql parameters</param>
        /// <param name="mapToModel">Mapping function that takes an IDataRecord and returns an of object of type T</param>
        /// <returns>Collection of objects of type T</returns>
        Task<ICollection<T>> ExecuteReadStoredProcedureAsync<T>(string storedProcedure, IEnumerable<System.Data.SqlClient.SqlParameter> parameters, Func<IDataRecord, T> mapToModel);
    }
}