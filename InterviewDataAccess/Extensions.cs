﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterviewDataAccess
{
    public static class Extensions
    {
        public static DateTime? ToDate(this string dateString)
        {
            var dateParts = dateString.Split('/');

            if (dateParts.Length != 3)
            {
                return null;
            }

            int.TryParse(dateParts[0], out int month);
            int.TryParse(dateParts[1], out int day);
            int.TryParse(dateParts[2], out int year);

            return new DateTime(year, month, day);
        }
    }
}
