﻿using DataStorage;
using DataStorage.Repositories;
using Domain.Exceptions;
using System;
using System.Threading.Tasks;

namespace InterviewDataAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            var connectionString = @"Data Source=DESKTOP-ROLL\SQLEXPRESS;Initial Catalog=Test;Integrated Security=True";

            var databaseClient = new SqlDatabaseClient(connectionString);

            var ordersRepository = new OrdersRepository(databaseClient);

            Console.WriteLine("Enter date in format of mm/dd/yyyy: ");
            var input = Console.ReadLine();
            try
            {
                var date = input.ToDate();

                if (date == null)
                {
                    Console.WriteLine("Invalid date input");
                }
                else
                {
                    var people = await ordersRepository.GetPeopleWithOrdersOnDate(date ?? new DateTime());
                    foreach (var person in people)
                    {
                        Console.WriteLine($"{person.PersonId}  {person.FirstName}  {person.LastName}");
                    }
                }
            }
            catch (RepositoryException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            Console.ReadLine();
        }
    }
}