﻿using System.Data;

namespace DataStorage
{
    /// <summary>
    /// DataStorage related extensions
    /// </summary>
    public static class DataStorageExtensions
    {
        /// <summary>
        /// Tries to get a value of type T from a column with name columnName and returns default is the value is not there
        /// </summary>
        /// <typeparam name="T">The expected type of return</typeparam>
        /// <param name="dataRecord">Data record</param>
        /// <param name="columnName">Column name</param>
        /// <returns>The value of type T</returns>
        public static T TryToGetValue<T>(this IDataRecord dataRecord, string columnName)
        {
            if (string.IsNullOrEmpty(columnName))
            {
                return default(T);
            }

            var ordinal = dataRecord.GetOrdinal(columnName);

            if (dataRecord.IsDBNull(ordinal))
            {
                return default(T);
            }

            return (T)dataRecord.GetValue(ordinal);
        }
    }
}
