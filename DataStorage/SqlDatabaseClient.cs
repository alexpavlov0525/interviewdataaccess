﻿using Domain.DataStorage;
using Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataStorage
{
    /// <summary>
    /// Sql database client
    /// </summary>
    public class SqlDatabaseClient : IDatabaseClient
    {
        /// <summary>
        /// Connection string
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// Costructor of SqlDataBaseClient
        /// </summary>
        /// <param name="connectionString">Conneciton string</param>
        public SqlDatabaseClient(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException($"{nameof(connectionString)} can not be null or empty");
            }

            this.connectionString = connectionString;
        }

        /// <summary>
        /// Executes a stored procedure which is suppose to read data.
        /// </summary>
        /// <typeparam name="T">Type of the objects in returned collection</typeparam>
        /// <param name="storedProcedure">Stored procedure name</param>
        /// <param name="parameters">Collection of Sql parameters</param>
        /// <param name="mapToModel">Mapping function that takes an IDataRecord and returns an of object of type T</param>
        /// <returns>Collection of objects of type T</returns>
        public async Task<ICollection<T>> ExecuteReadStoredProcedureAsync<T>(string storedProcedure, IEnumerable<System.Data.SqlClient.SqlParameter> parameters, Func<IDataRecord, T> mapToModel)
        {
            if (!StoredProcedures.Exists(storedProcedure))
            {
                throw new SqlDatabaseClientException($"{nameof(storedProcedure)} has to be a known stored procedure");
            }

            var resultCollection = new List<T>();

            try
            {
                using (var sqlConnection = new SqlConnection(this.connectionString))
                {
                    await sqlConnection.OpenAsync();
                    using (var command = new SqlCommand(storedProcedure, sqlConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters.ToArray());
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                try
                                {
                                    resultCollection.Add(mapToModel(reader));
                                }
                                catch (Exception exception)
                                {
                                    throw new MappingFunctionException($"{nameof(mapToModel)} has failed.", exception);
                                }
                            }
                        }
                    }

                }
            }
            catch (MappingFunctionException exception)
            {
                throw new SqlDatabaseClientException($"{nameof(SqlDatabaseClient)}:{nameof(ExecuteReadStoredProcedureAsync)} has failed.", exception);
            }
            catch (Exception exception)
            {
                throw new SqlDatabaseClientException($"{nameof(SqlDatabaseClient)}:{nameof(ExecuteReadStoredProcedureAsync)} has failed.", exception);
            }

            return resultCollection;
        }
    }
}
