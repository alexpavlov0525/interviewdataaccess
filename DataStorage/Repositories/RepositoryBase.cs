﻿using System;

namespace DataStorage.Repositories
{
    public class RepositoryBase
    {
        protected readonly IDatabaseClient databaseClient;

        public RepositoryBase(IDatabaseClient databaseClient)
        {
            this.databaseClient = databaseClient ??
                throw new ArgumentException($"{databaseClient} can not be null");
        }
    }
}