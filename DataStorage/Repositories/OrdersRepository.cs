﻿using Domain;
using Domain.DataStorage;
using Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DataStorage.Repositories
{
    /// <summary>
    /// Orders Repository
    /// </summary>
    public class OrdersRepository : RepositoryBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="databaseClient"></param>
        public OrdersRepository(IDatabaseClient databaseClient) : base (databaseClient)
        {
        }

        /// <summary>
        /// Get a collection of people who had orders on a provided date
        /// </summary>
        /// <param name="dateOfOrder">Date to check orders on</param>
        /// <returns>A collection of Person objects</returns>
        public async Task<ICollection<Person>> GetPeopleWithOrdersOnDate(DateTime dateOfOrder)
        {
            if (dateOfOrder == null)
            {
                throw new ArgumentException($"{nameof(dateOfOrder)} can not be null");
            }

            var collectionToReturn = new List<Person>();

            try
            {
                var paramtersCollection = new List<System.Data.SqlClient.SqlParameter> { new System.Data.SqlClient.SqlParameter("@orderDate", dateOfOrder) };
                collectionToReturn = (await this.databaseClient.ExecuteReadStoredProcedureAsync(StoredProcedures.GetPeopleWithOrderOnDate, paramtersCollection, MapToPerson))?.ToList();
            }
            catch (SqlDatabaseClientException exception)
            {
                throw new RepositoryException($"{nameof(OrdersRepository)}:{nameof(GetPeopleWithOrdersOnDate)} has failed.", exception);
            }
            catch (MappingFunctionException exception)
            {
                throw new RepositoryException($"{nameof(OrdersRepository)}:{nameof(GetPeopleWithOrdersOnDate)} has failed.", exception);
            }
            catch (Exception exception)
            {
                throw new RepositoryException($"{nameof(OrdersRepository)}:{nameof(GetPeopleWithOrdersOnDate)} has failed.", exception);
            }

            return collectionToReturn;
        }

        /// <summary>
        /// Maps a data record top a Person object
        /// </summary>
        /// <param name="dataRecord">Data record</param>
        /// <returns>Person</returns>
        private Person MapToPerson(IDataRecord dataRecord)
        {
            return new Person
            {
                PersonId = dataRecord.TryToGetValue<int>("PersonId"),
                FirstName = dataRecord.TryToGetValue<string>("NameFirst"),
                LastName = dataRecord.TryToGetValue<string>("NameLast")
            };
        }
    }
}
